<?php


include_once('WriteData.php');


// Getting the SkierLogs.XML
$doc = new DOMDocument();
$doc->load( 'SkierLogs.xml' );


  $insert = new WriteData();


//Getting Club
$xPath = new DOMXpath($doc);
$clubs = $xPath->query('/SkierLogs/Clubs/Club');

foreach($clubs AS $club) {
//Get club id
    $clubId = $club->getAttribute('id');
//Get club name
    $node = $club->getElementsByTagName('Name');
    $clubName = $node->item(0)->textContent;
//Get club city
    $node =$club->getElementsByTagName('City');
    $clubCity = $node->item(0)->textContent;
//Get club county
    $node = $club->getElementsByTagName('County');
    $clubCounty = $node->item(0)->textContent;

//Insert into database
    $insert->insertClubs($clubId, $clubName, $clubCity, $clubCounty);
}






//Getting Skiers
$skiers = $xPath->query('/SkierLogs/Skiers/Skier');

//To avoid getting clubs and skiers mixed up when displaying
$res = array();

foreach($skiers AS $skier) {
//Getting username
    $userName = $skier->getAttribute('userName');
//Getting first name
    $node = $skier->getElementsByTagName('FirstName');
    $fName = $node->item(0)->textContent;
//Getting last name
    $node = $skier->getElementsByTagName('LastName');
    $lName = $node->item(0)->textContent;
//Getting birth year
    $node = $skier->getElementsByTagName('YearOfBirth');
    $bYear = $node->item(0)->textContent;
    $bYearInt = (int)$bYear;

//Insert into database
    $insert->insertSkiers($userName, $fName, $lName, $bYearInt);

}





//To get entries and total distance/skierSeasonClub

$seasons=$xPath->query('/SkierLogs/Season');
//Get fallyear
foreach($seasons as $season){
    $seasonFallYear=$season->getAttribute('fallYear');
//Get clubID
    foreach($season->getElementsByTagName('Skiers') as $seasonClub) {
        $seasonClubId=$seasonClub->getAttribute('clubId');
//Get userName
        /** @var DOMElement $seasonUser */
        foreach($seasonClub->getElementsByTagName('Skier') as $seasonUser) {
            $seasonUserName=$seasonUser->getAttribute('userName');
//Go through Log
            $logElement = $seasonUser->getElementsByTagName('Log')->item(0);
            $totalDistance = 0;
//Get Distance for totalDistance
            foreach($logElement->getElementsByTagName('Entry') as $entry) {
                $distance = (int)$entry->getElementsByTagName('Distance')->item(0)->textContent;
                $totalDistance += $distance;
            }
//Insert SkierSeasonClub into database
                $insert->insertSkierSeasonClub($seasonUserName, $totalDistance, $seasonClubId, $seasonFallYear);
//Get Distance for Entry
            foreach($logElement->getElementsByTagName('Entry') as $entry) {
                $node=$entry->getElementsByTagName('Distance');
                $distance = (int)$node->item(0)->textContent;
//Get date
                $node=$entry->getElementsByTagName('Date');
                $eDate = $node->item(0)->textContent;
//Insert Entries into database
                $insert->insertEntries($seasonFallYear, $eDate, $distance, $seasonUserName);
            }
        }
    }
}


?>