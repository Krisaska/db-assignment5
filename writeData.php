<?php


class WriteData {
    protected $db;

    public function __construct(){
        // Create PDO connection
        $this->db = new PDO('mysql:host=localhost;dbname=skierlogs;charset=utf8mb4;',
            'root' , 'root');
    }


//Insert club into database
    public function insertClubs($clubId, $clubName, $city, $county) {

        $stmt = $this->db->prepare(
            "INSERT INTO Club (clubId, clubName, city, county)  
                VALUES(:clubId, :clubName, :city, :county)");

        $stmt->bindValue(':clubId',   $clubId);
        $stmt->bindValue(':clubName', $clubName);
        $stmt->bindValue(':city',     $city);
        $stmt->bindValue(':county',   $county);
        $stmt->execute();
    }


//Insert skiers into database
    public function insertSkiers($userName, $fName, $lName, $BD) {
        $stmt = $this->db->prepare("INSERT INTO skiers (userName, firstName, lastName, yearOfBirth) 
				VALUES (:userName, :fName, :lName, :BD)");

        $stmt->bindValue(':userName',    $userName);
        $stmt->bindValue(':firstName',   $fName);
        $stmt->bindValue(':lastName',    $lName);
        $stmt->bindValue(':birthOfYear', $BD);
        $stmt->execute();
    }


//Insert entries to database
    public function  insertEntries($fallYear, $eDate, $distance, $userName) {
     $stmt = $this->db->prepare("INSERT INTO Entry (Season, eDate, distance, userName)
                VALUES (:fallYear, :eDate, :distance, :userName)");

        $stmt->bindValue('Season',   $fallYear);
        $stmt->bindValue('eDate',    $eDate);
        $stmt->bindValue('distance', $distance);
        $stmt->bindValue('userName', $userName);
        $stmt->execute();
    }


//Insert total distance / SkierSeasonClub into database
    public function insertSkierSeasonClub($userName, $totalDistance, $clubId, $season) {
        $stmt = $this->db->prepare("INSERT INTO SkierSeasonClub (userName, totalDistance, clubId, season)
                VALUES (:userName, :totalDistance, :clubId, :season)");

        $stmt->bindValue('userName', $userName);
        $stmt->bindValue('totalDistance', $totalDistance);
        $stmt->bindValue('clubId', $clubId);
        $stmt->bindValue('season', $season);
        $stmt->execute();
}


}

?>