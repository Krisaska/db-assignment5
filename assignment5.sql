-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 02, 2017 at 04:49 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Assignment5`
--

-- --------------------------------------------------------

--
-- Table structure for table `Club`
--

CREATE TABLE `Club` (
  `clubId` varchar(60) COLLATE utf8_danish_ci NOT NULL,
  `clubName` varchar(60) COLLATE utf8_danish_ci NOT NULL,
  `city` varchar(60) COLLATE utf8_danish_ci NOT NULL,
  `county` varchar(60) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Entry`
--

CREATE TABLE `Entry` (
  `season` int(11) NOT NULL,
  `eDate` datetime NOT NULL,
  `distance` int(11) NOT NULL,
  `userName` varchar(60) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Skier`
--

CREATE TABLE `Skier` (
  `userName` varchar(60) COLLATE utf8_danish_ci NOT NULL,
  `firstName` varchar(60) COLLATE utf8_danish_ci NOT NULL,
  `lastName` varchar(60) COLLATE utf8_danish_ci NOT NULL,
  `yearOfBirth` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SkierSeasonClub`
--

CREATE TABLE `SkierSeasonClub` (
  `userName` varchar(60) COLLATE utf8_danish_ci NOT NULL,
  `totalDistance` int(11) NOT NULL,
  `clubId` varchar(60) COLLATE utf8_danish_ci NOT NULL,
  `season` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Club`
--
ALTER TABLE `Club`
  ADD PRIMARY KEY (`clubId`);

--
-- Indexes for table `Entry`
--
ALTER TABLE `Entry`
  ADD PRIMARY KEY (`season`),
  ADD KEY `userName` (`userName`);

--
-- Indexes for table `Skier`
--
ALTER TABLE `Skier`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `SkierSeasonClub`
--
ALTER TABLE `SkierSeasonClub`
  ADD PRIMARY KEY (`userName`),
  ADD KEY `clubId` (`clubId`),
  ADD KEY `season` (`season`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Entry`
--
ALTER TABLE `Entry`
  ADD CONSTRAINT `entry_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `SkierSeasonClub` (`userName`) ON UPDATE CASCADE;

--
-- Constraints for table `SkierSeasonClub`
--
ALTER TABLE `SkierSeasonClub`
  ADD CONSTRAINT `skierseasonclub_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `Skier` (`userName`) ON UPDATE CASCADE,
  ADD CONSTRAINT `skierseasonclub_ibfk_2` FOREIGN KEY (`clubId`) REFERENCES `Club` (`clubId`) ON UPDATE CASCADE,
  ADD CONSTRAINT `skierseasonclub_ibfk_3` FOREIGN KEY (`season`) REFERENCES `Entry` (`season`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
